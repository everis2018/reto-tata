'use strict';

const mongoose = require('mongoose');
const Promise = require('bluebird');
const validator = require('validator');
const VehiculoModel = require('./../model/Vehiculo.js');


mongoose.Promise = Promise;

const mongoString = 'mongodb://localhost:27017/red_bicicletas'; // MongoDB Url

const createErrorResponse = (statusCode, message) => ({
  statusCode: statusCode || 501,
  headers: { 'Content-Type': 'text/plain' },
  body: message || 'Incorrect id',
});

const dbExecute = (db, fn) => db.then(fn).finally(() => db.close());

function dbConnectAndExecute(dbUrl, fn) {
  return dbExecute(mongoose.connect(dbUrl, { useNewUrlParser: true }), fn);
}

module.exports.crearVehiculo = (event, context, callback) => {
  const data = JSON.parse(event.body);
  const vehiculo = new VehiculoModel({
    capacidadcarga: data.capacidadcarga,
    consumibles: data.consumibles,
    costoencreditos: data.costoencreditos,
    tripulacion: data.tripulacion,
    editado: data.editado,
    longitud: data.longitud,
    fabricante: data.fabricante,
    velocidadmaximadeatmosfera: data.velocidadmaximadeatmosfera,
    modelo: data.modelo,
    nombre: data.nombre,
    pasajeros: data.pasajeros,
    url: data.url,
    clasedevehiculo: data.clasedevehiculo,
  });

  if (vehiculo.validateSync()) {
    callback(null, createErrorResponse(400, 'Incorrect user data'));
    return;
  }

  dbConnectAndExecute(mongoString, () => (
    vehiculo
      .save()
      .then(() => callback(null, {
        statusCode: 200,
        body: JSON.stringify({ id: vehiculo.id }),
      }))
      .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))
  ));
};


module.exports.obtenerVehiculo = (event, context, callback) => {
  if (!validator.isAlphanumeric(event.pathParameters.id)) {
    callback(null, createErrorResponse(400, 'Incorrect id'));
    return;
  }

  dbConnectAndExecute(mongoString, () => (
    VehiculoModel
      .find({ _id: event.pathParameters.id })
      .then(vehiculo => callback(null, { statusCode: 200, body: JSON.stringify(vehiculo) }))
      .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))
  ));
};

const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const validator = require('validator');

const VehiculoSchema  = new Schema({
  capacidadcarga: {
    type: String,
    required: true,
    validate: {
      validator(capacidadcarga) {
        return validator.isAlphanumeric(capacidadcarga);
      },
    },
  },
  consumibles: {
    type: String,
    required: true,
    validate: {
      validator(consumibles) {
        return validator.isAlphanumeric(consumibles);
      },
    },
  },
  costoencreditos: {
    type: String,
    required: true,
    validate: {
      validator(costoencreditos) {
        return validator.isAlphanumeric(costoencreditos);
      },
    },
  },
  creado: {
    type: Date,
    required: true,
    default: Date.now,
  },
  tripulacion: {
    type: String,
    required: true,
    validate: {
      validator(tripulacion) {
        return validator.isAlphanumeric(tripulacion);
      },
    },
  },
  editado: {
    type: Date,
    required: true,
  },
  longitud: {
    type: String,
    required: true,
    validate: {
      validator(longitud) {
        return validator.isAlphanumeric(longitud);
      },
    },
  },
  fabricante: {
    type: String,
    required: true,
    validate: {
      validator(fabricante) {
        return validator.isAlphanumeric(fabricante);
      },
    },
  },
  velocidadmaximadeatmosfera: {
    type: String,
    required: true,
    validate: {
      validator(velocidadmaximadeatmosfera) {
        return validator.isAlphanumeric(velocidadmaximadeatmosfera);
      },
    },
  },
  modelo: {
    type: String,
    required: true,
    validate: {
      validator(modelo) {
        return validator.isAlphanumeric(modelo);
      },
    },
  },
  nombre: {
    type: String,
    required: true,
    validate: {
      validator(nombre) {
        return validator.isAlphanumeric(nombre);
      },
    },
  },
  pasajeros: {
    type: String,
    required: true,
    validate: {
      validator(pasajeros) {
        return validator.isAlphanumeric(pasajeros);
      },
    },
  },
  url: {
    type: String,
    required: true,
    validate: {
      validator(url) {
        return validator.isAlphanumeric(url);
      },
    },
  },
  clasedevehiculo: {
    type: String,
    required: true,
    validate: {
      validator(clasedevehiculo) {
        return validator.isAlphanumeric(clasedevehiculo);
      },
    },
  },  
});

module.exports = mongoose.model('Vehiculo', VehiculoSchema);

